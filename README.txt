XMLsitemap PDFs
======================

This project will add PDFs to the XML Sitemap.

Installation
------------

- Install this module using the official Drupal instructions at
  https://www.drupal.org/docs/7/extend/installing-modules

- Visit the configuration page under Administration > Configuration >
  Search & Metadata > XMLsiteamp (admin/config/search/xmlsitemat) and
  enter the required information.


Issues
------

Bugs and Feature requests should be reported in the Issue Queue:
https://www.drupal.org/project/issues/xmlsitemap_pdf?categories=All


License
-------

This project is GPL v2 software.
See the LICENSE.txt file in this directory for complete text.
