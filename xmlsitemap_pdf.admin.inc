<?php
/**
 * @file
 * Admin pages for the XMLsitemap PDFs module.
 */

/**
 * Settings form.
 */
function xmlsitemap_pdf_settings_form($form, &$form_state) {
  $form = array();

  $links_default = variable_get('xmlsitemap_pdf_links', array('pdf' => 'pdf'));
  $form['xmlsitemap_pdf_links'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Types of links to include'),
    '#options' => array(
      'pdf' => t('Links to the PDF documents: <em>Example: https://www.ex.com/sites/default/files/document.pdf</em>'),
      'file' => t('Links to the File entity pages: <em>Example: https://www.ex.com/file/1234</em>'),
    ),
    '#default_value' => $links_default,
  );

  $freq_options = xmlsitemap_get_changefreq_options();
  $changefreq_default = variable_get('xmlsitemap_pdf_changefreq', '2419200');
  $form['xmlsitemap_pdf_changefreq'] = array(
    '#type' => 'select',
    '#title' => t('Change frequency'),
    //'#description' => t('When this value changes all PDFs in the sitemap will be updated.'), @todo
    '#options' => $freq_options,
    '#default_value' => $changefreq_default,
  );

  $form['info'] = array(
    '#type' => 'fieldset',
    '#title' => t('PDF links in XML Sitemap'),
  );

  $header = array(
    t('Links to:'),
    t('Number of links'),
    t('Priority'),
    t('Change frequency'),
    t('Remove links from XML Sitemap'),
  );


  $rows = array();

  $filetypes = xmlsitemap_pdf_get_filetypes();
  foreach ($filetypes as $bundle => $type) {
    $type_plural = $type . 's';

    $freq_var = variable_get('xmlsitemap_pdf_changefreq', '2419200');
    $freq = '';
    $pdf_remove_link = $page_remove_link = '';

    $pdf_count = db_query("SELECT COUNT(id) FROM {xmlsitemap}
      WHERE type = 'pdf' AND subtype = :sub AND access = 1 AND status = 1",
      array(':sub' => $bundle))->fetchField();
    if ($pdf_count) {
      $freq = xmlsitemap_get_changefreq($freq_var);
      $pdf_remove_link = l(t('Remove PDF document links for @type', array('@type' => $type_plural)),
        'admin/config/search/xmlsitemap/pdfs/remove/pdf');
    }

    $settings = xmlsitemap_link_bundle_load('file', $bundle);
    $page_count = db_query("SELECT COUNT(id) FROM {xmlsitemap}
      WHERE type = 'file' AND subtype = :sub AND access = 1 AND status = 1",
      array(':sub' => $bundle))->fetchField();
    if ($page_count) {
      $freq = xmlsitemap_get_changefreq($freq_var);
      $page_remove_link = l(t('Remove File entity links for @type', array('@type' => $type_plural)),
        'admin/config/search/xmlsitemap/pdfs/remove/file');
    }

    $rows[$bundle . '-pdf'] = array(
      t($type)  . ' - ' .  '<strong>' . t('PDF documents') . '</strong>',
      $pdf_count,
      $settings['priority'],
      $freq,
      $pdf_remove_link,
    );

    $rows[$bundle . '-file'] = array(
      t($type) . ' - ' . '<strong>' . t('File entity pages') . '</strong>',
      $page_count,
      $settings['priority'],
      $freq,
      $page_remove_link,
    );

  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));

  $form['info']['count'] = array(
    '#type' => 'markup',
    '#markup' => $output,
  );

  return system_settings_form($form);
}



/**
 * Confirm form for removing PDF links from the sitemap.
 */
function xmlsitemap_pdf_links_remove($form, &$form_state) {
  $form = array();

  $type = arg(6);
  $cancel_path = 'admin/config/search/xmlsitemap/pdfs';

  return confirm_form(
    $form,
    t('Are you sure you want to delete the %type links?', array('%type' => $type)),
    $cancel_path,
    t('It will take many cron runs to get all the links added back into the XML sitemap.'),
    t('Remove'),
    t('Cancel'));
}

/**
 * Submit handler for Settings form.
 */
function xmlsitemap_pdf_links_remove_submit(&$form, &$form_state) {
  if ($form_state['values']['submit'] == 'Remove') {
    $type = arg(6);

    db_query("DELETE from {xmlsitemap} WHERE type = :type", array(':type' => $type));

    $message = t('The %type links have been removed from the XML sitemap.', array('%type' => $type));
    drupal_set_message($message);
    drupal_goto('admin/config/search/xmlsitemap/pdfs');
  }
}
